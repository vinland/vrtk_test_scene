﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorFlip : MonoBehaviour {

	Camera cam;

	private void Start()
	{
		cam = GetComponent<Camera>();
	}

	private void OnPreCull()
	{
		cam.ResetWorldToCameraMatrix();
		cam.ResetProjectionMatrix();
		cam.projectionMatrix = cam.projectionMatrix * Matrix4x4.Scale(new Vector3(-1, 1, 1));
	}

	private void OnPreRender()
	{
		GL.invertCulling = true;
	}

	private void OnPostRender()
	{
		GL.invertCulling = false;
	}
}
