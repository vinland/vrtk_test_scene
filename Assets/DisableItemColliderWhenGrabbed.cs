﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class DisableItemColliderWhenGrabbed : MonoBehaviour 
{
	VRTK_InteractableObject interactableObject;

	private void Start()
	{
		interactableObject = GetComponent<VRTK_InteractableObject>();
		interactableObject.InteractableObjectGrabbed += Grabbed;
		interactableObject.InteractableObjectUngrabbed += UnGrabbed;
	}

	void Grabbed(object sender, InteractableObjectEventArgs e)
	{
		gameObject.layer = LayerMask.NameToLayer("GrabbedObject");
	}

	void UnGrabbed(object sender, InteractableObjectEventArgs e)
	{
		gameObject.layer = LayerMask.NameToLayer("Default");
	}
}
